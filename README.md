## Requirements ##

* [PHP >= 7.1](Link http://php.net)

## Documentations ##



## Quick Installation ##


```
#!composer

$ composer require oteck/ecommerce:^1.0
```

## Configuration ##

### Environment configurations (.env) ###
Configure the URL to access the CMS Manager.
```
#!php

APP_URL=https://manager.example.com
```

### Debugging Mode ###

To enable debugging mode, set **APP_DEBUG=true** in your .env file.

**IMPORTANT:** Please make sure that APP_DEBUG is set to false when your app is on production.

### Service Provider ###
Add the following class under the "**providers**" key where the comment heading "**Package Service Providers...**" is written:

```
#!php

Oteck\Ecommerce\CmsServiceProvider::class
```

## Contributing ##

## Security ##

## Credits ##

## License ##